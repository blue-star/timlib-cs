﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace TimLib.Utility
{ 
	public class Signal
	{
		public delegate void SignalEvent(dynamic argument);

		private List<SignalEvent> always, once;

		/**
		*Create a new empty Signal.
		* */
		public Signal()
		{
			always = new List<SignalEvent>();
			once = new List<SignalEvent>();
		}

		/**
		*Trigger all events attached to this Signal.
		*param argument: the value sent to the events added to this Signal.
		* */
		public void Dispatch(dynamic argument = null)
		{
			if (always.Count > 0)
			{
				for (int i = 0; i < always.Count; i++)
				{
					always[i](argument);
				}
			}

			if (once.Count > 0)
			{
				for (int i = 0; i < always.Count; i++)
				{
					once[i](argument);
				}
			}

			once = new List<SignalEvent>();
		}

		/**
		*Add a new event to be triggered when this Signal is dispatched.
		* */
		public void Add(SignalEvent e)
		{
			always.Add(e);
		}

		/**
		*Add a new event to be triggered when this Signal is dispatched.
		*Events added this way are only triggered a single time.
		* */
		public void AddOnce(SignalEvent e)
		{
			once.Add(e);
		}

		/**
		*Remove all attached events from this Signal.
		* */
		public void ClearAll()
		{
			always = new List<SignalEvent>();
			once = new List<SignalEvent>();
		}
	}
}
